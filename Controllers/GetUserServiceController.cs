﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCredytyDB.Business;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Controllers
{
    [Route("api/GetUserService")]
    [ApiController]
    public class GetUserServiceController : ControllerBase
    {
        /// <summary>
        /// Method used to get all users
        /// </summary>
        /// <returns>User list</returns>
        [HttpGet]
        [Route("GetAllUsers")]
        public UserResponse GetAllUsers()
        {
            return GetUserServiceManager.Instance.GetAllUsers();
        }

        /// <summary>
        /// Method used to get a specific user by email
        /// </summary>
        /// <param name="email">Email to vaildate</param>
        /// <returns>User result</returns>
        [HttpGet]
        [Route("GetUser/{userId}")]
        public UserResponse GetUser(int userId)
        {
            return GetUserServiceManager.Instance.GetUser(userId);
        }
    }
}

﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Respository
{
    public class UserRepository
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static UserRepository instance;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static UserRepository Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }
                instance = new UserRepository();
                return instance;
            }
        }

        /// <summary>
        /// Get all users in the data base
        /// </summary>
        /// <returns>List of users</returns>
        public List<User> GetAllUsers()
        {
            var userList = new List<User>();
            var connection = RepositoryManager.Instance.OpenConnection();
            using (connection)
            {                
                using (var cmd = new NpgsqlCommand("fn_get_all_users", connection))
                {                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                userList.Add(
                                    new User
                                    {
                                        UserId = Convert.ToInt32(reader["id"]),
                                        Name = Convert.ToString(reader["name"]),
                                        Email = Convert.ToString(reader["email"]),
                                        Password = Convert.ToString(reader["password"])
                                    }
                                );
                            }
                        }
                    }
                }                                
            }

            return userList;
        }

        /// <summary>
        /// Get user data by specific email
        /// </summary>
        /// <param name="email">Email to validate</param>
        /// <returns>User data</returns>
        public User GetUser(int userId)
        {
            var user = new User();
            var connection = RepositoryManager.Instance.OpenConnection();
            using (connection)
            {                
                using (var cmd = new NpgsqlCommand("fn_get_user", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@validuserid", userId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                user = new User
                                {
                                    UserId = Convert.ToInt32(reader["id"]),
                                    Name = Convert.ToString(reader["name"]),
                                    Email = Convert.ToString(reader["email"]),
                                    Password = Convert.ToString(reader["password"])
                                };
                            }
                        }
                    }
                }
            }
            return user;
        }
    }
}

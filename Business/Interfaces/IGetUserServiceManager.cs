﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Business.Interfaces
{
    interface IGetUserServiceManager
    {
        /// <summary>
        /// Method to get all users
        /// </summary>
        /// <returns>User list</returns>
        UserResponse GetAllUsers();

        /// <summary>
        /// Method used to get the user data by specific email
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>User data</returns>
        UserResponse GetUser(int userId);
    }
}
